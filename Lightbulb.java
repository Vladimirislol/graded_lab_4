import java.util.Scanner;
public class Lightbulb {
	private int watts;
	private boolean state;
	private String brand;
	
	public Lightbulb(int initWatts, boolean initState, String initBrand) {
		this.watts = initWatts;
		this.state = initState;
		this.brand = initBrand;
	}
	public int getWatts() {
		return this.watts;
	}
	public void setWatts(int newWatts) {
		this.watts = newWatts;
	}
	public boolean getState() {
		return this.state;
	}
	public void setState(boolean newState) {
		this.state = newState;
	}
	public String getBrand() {
		return this.brand;
	}
	public void setBrand(String newBrand) {
		this.brand = newBrand;
	}
	public void isOn(boolean currentState) {
		if(currentState) {
			System.out.println("This lightbulb is on.");
		} else {
			System.out.println("This lightbulb is on.");
		}
	}
	public void printBrand(String currentBrand) {
		System.out.println("This lightbulb's brand is "+currentBrand+".");
	}
	public void changeBrand() {
		Scanner sc = new Scanner(System.in);
		String newBrand = "";
		boolean valid = false;
		do {
			System.out.println("let's change the brand of your bulb!");
			newBrand = sc.nextLine();
			valid = validity(newBrand);
		}while (!(valid));
	}
	private boolean validity(String newBrand) {
		String ugly = "ugly";
		if (newBrand.equals(ugly)) {
				System.out.println("that's not a nice brand :( please put in a different brand");
				return false;
			} else {
				this.brand = newBrand;
				System.out.println("your lightbulb's brand has been changed to "+newBrand+".");
				return true;
			}
	}
}