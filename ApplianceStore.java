import java.util.Scanner;
public class ApplianceStore {
	public static void main(String[] args) {
		Lightbulb[] lightbulb = new Lightbulb[2];
		Scanner sc = new Scanner(System.in);
		String truth = "true";
		String isTrue = "";
		String initBrand = "";
		int initWatts = 0;
		boolean initState = false;
		for(int i = 0; i < lightbulb.length; i++) {
			
			System.out.println("please input wether the bulb is on or off for lightbulb # "+(i+1)+" [true/false].");
			isTrue = sc.nextLine();
			if (isTrue.equals(truth)) {
				initState = true;
			} else {
				initState = false;
			}
			
			System.out.println("please input the number of watts for lightbulb # "+(i+1)+" [integer].");
			initWatts = Integer.parseInt(sc.nextLine());
			
			System.out.println("please input the brand for lightbulb # "+(i+1)+" [String].");
			initBrand = sc.nextLine();
			
			lightbulb[i] = new Lightbulb(initWatts, initState, initBrand);
		}
		
		System.out.println("the last lightbulb is on: "+lightbulb[1].getState()+". lightbulb has a capacity of "+lightbulb[1].getWatts()+" watts. It's brand is "+lightbulb[1].getBrand()+".");
		System.out.println("it seems like a mistake was made with the last bulb. Let's fix it! what is the state of the last bulb? [true/false].");
		isTrue = sc.nextLine();
			if (isTrue.equals(truth)) {
				initState = true;
			} else {
				initState = false;
			}
		lightbulb[1].setState(initState);
		System.out.println("please input the number of watts for this bulb [integer].");
		initWatts = Integer.parseInt(sc.nextLine());
		lightbulb[1].setWatts(initWatts);
		System.out.println("please input the brand for lthis bulb [String].");
		initBrand = sc.nextLine();
		lightbulb[1].setBrand(initBrand);
		System.out.println("now that we have fixed the wrong information with lughtbulb 2, let's check what it's fields are: ");
		System.out.println("The last bulb's current state (on/off) is: "+lightbulb[1].getState()+". it has a watts capacity of "+lightbulb[1].getWatts()+" and is from the brand "+lightbulb[1].getBrand()+".");
		
	}
}